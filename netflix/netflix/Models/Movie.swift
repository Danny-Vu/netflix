//
//  Movie.swift
//  netflix
//
//  Created by Bao Vu on 6/12/22.
//

import Foundation

struct TrendingMoviesResponse: Codable {
    let results: [Movie]
}

struct Movie: Codable {
    let id: Int
    let media_type: String?
    let name: String?
    let orginal_name: String?
    let poster_path: String?
    let overview: String?
    let vote_count: Int?
    let release_date: String?
    let vote_average: Double?
    let popularity: Double?
}
 
